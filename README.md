# thesis

Test task. Calendar vue component.

[demo](https://asleeppiano.gitlab.io/calendar-test-component)

## Local development

``` sh
git clone git@gitlab.com:asleeppiano/calendar-test-component.git
cd calendar-test-component
npm install
npm run dev
```
