import type { EventType } from '@/const';

type DateString = string;

export type Event = {
    id: number;
    name: string;
    date: DateString;
    type: EventType;
};

export type Events = Array<Event>;

export type CellDate = {
    date: number;
    day: number;
    events: Events;
};

export type Dates = Array<CellDate>;
